const { getById, remove } = require('../repositories/cardRepository')
const { errorParams } = require('../exception/errorParams')

module.exports.handler = async (event) => {
   try {
      const id = event.pathParameters.id
      const existCard = await getById(id)
      
      if (existCard) {
         await remove(id)

         return {
            statusCode: 200,
            body: JSON.stringify(`Card has been deleted.`)
         }
      } else {
         return {
            statusCode: 404,
            body: JSON.stringify('Card not found. Please check your card id and try again')
         }
      }
   } catch(error) {
      const params = errorParams(error)

      return {
         statusCode: params.statusCode,
         body: params.body
      }
   }
}