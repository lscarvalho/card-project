const { getAll } = require('../repositories/cardRepository')
const { errorParams } = require('../exception/errorParams')

module.exports.handler = async () => {
   try {
      const cards = await getAll()

      if (cards.length > 0) {
         return {
            statusCode: 200,
            body: JSON.stringify(cards)
         }
      } else {
         return {
            statusCode: 404,
            body: JSON.stringify("Card list is empty!")
         }
      }
   } catch(error) {
      const params = errorParams(error)

      return {
         statusCode: params.statusCode,
         body: params.body
      }
   }
}