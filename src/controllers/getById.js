const { getById } = require('../repositories/cardRepository')
const { errorParams } = require('../exception/errorParams')

module.exports.handler = async (event) => {
   try {
      const id = event.pathParameters.id
      const card = await getById(id)

      if (card) {
         return {
            statusCode: 200,
            body: JSON.stringify(card)
         }
      } else {
         return {
            statusCode: 404,
            body: JSON.stringify('Card not found. Please check your card id and try again')
         }
      }
   } catch(error) {
      const params = errorParams(error)

      return {
         statusCode: params.statusCode,
         body: params.body
      }
   }
}