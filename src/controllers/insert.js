const { insert } = require('../repositories/cardRepository')
const { errorParams } = require('../exception/errorParams')

module.exports.handler = async (event) => {
   try {
      const body = JSON.parse(event.body)

      if(body.nameCli) {
         const newCard = await insert(body)
      
         return {
            statusCode: 201,
            body: JSON.stringify(newCard)
         }
      } else {
         return {
            statusCode: 400,
            body: JSON.stringify("Client name doesn't be empty.")
         }
      }    
   } catch(error) {
      const params = errorParams(error)

      return {
         statusCode: params.statusCode,
         body: params.body
      }
   }
}