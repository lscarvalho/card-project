const { getById, update } = require('../repositories/cardRepository')
const { errorParams } = require('../exception/errorParams')

module.exports.handler = async (event) => {
   try {
      const id = event.pathParameters.id
      const card = JSON.parse(event.body)

      const existCard = await getById(id)

      if (existCard) {
         if (card.nameCli) {
            await update(id, card)

            return {
               statusCode: 200,
               body: JSON.stringify(card)
            }
         } else {
            return {
               statusCode: 400,
               body: JSON.stringify("Client name doesn't be empty.")
            }
         }
      } else {
         return {
            statusCode: 404,
            body: JSON.stringify('Card not found. Please check your card id and try again')
         }
      }
   } catch(error) {
      const params = errorParams(error)

      return {
         statusCode: params.statusCode,
         body: params.body
      }
   }
}