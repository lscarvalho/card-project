module.exports.errorParams = (error) => {
   const statusCode = error.statusCode || 500
   const exception = error.name
   const message = error.message

   const params = {
      statusCode,
      body: JSON.stringify({ exception, message }, null, 2)
   }
   
   return params
}