const AWS = require('aws-sdk')

AWS.config.update({ region: 'us-east-1' })

const dynamoDb = new AWS.DynamoDB.DocumentClient()

const TableName = process.env.CARDS_TABLE

const { v4: uuidv4 } = require('uuid')

let generator = require('creditcard-generator')

const data = (card) => {
   const item = {
      'id': uuidv4(),
      'nameCli': card.nameCli,
      'numberCard': generator.GenCC(), // MASTERCARD
      'date': new Date().toLocaleString('pt-br', { timeZone: "America/Sao_Paulo" }),
      'balance': card.balance | 0
   }

   return item
}

const insert = async (card) => {
   const newCard = data(card)

   const params = {
      TableName,
      Item: newCard
   }

   await dynamoDb.put(params).promise()
   return newCard
}

const getAll = async () => {
   const params = { TableName }

   const items = await dynamoDb.scan(params).promise()
   
   return items.Items ? items.Items : undefined
}

const getAllByPagination = async (pageSize, lastItem) => {
   const params = {
      TableName,
      Limit: pageSize
   }

   if (lastItem) {
      params.ExclusiveStartKey = { id: lastItem }
   }

   const items = await dynamoDb.scan(params).promise()
   
   return items.Items ? items.Items : undefined
}

const getById = async (id) => {
   const params = {
      TableName,
      Key: { 'id': id }
   }

   const item = await dynamoDb.get(params).promise()

   return item.Item ? item.Item : undefined
}

const update = async (id, card) => {
   const params = {
      TableName,
      Key: { 'id': id },
      ConditionExpression: "id = :id",
      UpdateExpression: 'set nameCli = :nc, balance = :b',
      ExpressionAttributeValues: {
         ':id': id,
         ':nc': card.nameCli,
         ':b': card.balance
      },
      ReturnValues: 'UPDATED_NEW'
   }

   return await dynamoDb.update(params).promise()
}

const remove = async (id) => {
   const params = {
      TableName,
      Key: { 'id': id},
      ConditionExpression: 'id = :id',
      ExpressionAttributeValues: { ':id': id }
   }

   return await dynamoDb.delete(params).promise()
}

module.exports = {
   insert,
   getAll,
   getAllByPagination,
   getById,
   update,
   remove
}